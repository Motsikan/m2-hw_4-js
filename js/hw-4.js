// Теоретичні питання: ===========================================

// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
// --

// Функція - це частина коду, яку ми називаємо, 
// наповнюємо якимсь функціоналом, та зберігаємо в памяті, 
// а коли вона нам знадобиться, викликаємо.

// --------------------------------------------------------------
//  2. Описати своїми словами, навіщо у функцію передавати аргумент.
// --

// Тому що сама функція нічого не повертає,
// для цього необхідні аргументи, які передаються у функцію 
// і повертають якісь данні.

// --------------------------------------------------------------
// 3. Що таке оператор return та як він працює всередині функції?
// --

// За допомогою "return" ми можемо із функції щось повертати та зберігати данні.
// Після "return" функція автоматично припиняє своє виконання.

// ЗАВДАННЯ: =====================================================

let firstNumber;
let secondNumber;
let operator;

do {
    firstNumber = prompt ("Enter first number: ");
} while(isNaN(firstNumber) || firstNumber === "" || firstNumber === undefined || firstNumber === null);
    
    let a = Number (firstNumber);
    console.log(a);

do {
    secondNumber = prompt ("Enter second number: ");
} while(isNaN(secondNumber) || secondNumber === "" || secondNumber === undefined || secondNumber === null);

    let b = Number (secondNumber);
    console.log(b);

do {
    operator = prompt ("Choose an operation:  + ,  - ,  /  , *  ");
} while (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/") 
   
    let operation = operator;
    console.log(operation);

function calculate (a, b, operation) {       

        switch (operation) {
            case "+":
                return a + b;
                // console.log (a + b);
                // break;
            case "-": 
                console.log (a - b);
                return a - b;
                // break;
            case "*": 
                console.log (a * b);
                return a * b;
                // break;      
            case "/": 
                if(b === 0) {
                    console.error("Error!") ;
                    return;
                }
                console.log (a / b);
                return a / b;
            //    break;
            default:
                console.log ("Error!");               
        }   
    }
let result = (calculate(a, b, operation));
// console.log(result);
alert (`Calculation result = ${result}`);